<?php
namespace rental;
require_once("connect.php");
require_once("UsersController.php");
require_once("ComplainCommentsController.php");
require_once("ComplainUpvotesController.php");
require_once("ComplainsController.php");
require_once("RepliesController.php");

function index(){
    $action=$_GET['action'];
    
    //this section deals with users table, controlled by class UsersController
    if($action==='registeruser'){
        $db=getConnection();
        $name=mysqli_real_escape_string($db,$_GET['name']);
        $apartment=mysqli_real_escape_string($db,$_GET['apartment']);
        $contact=mysqli_real_escape_string($db,$_GET['contact']);
        $password=mysqli_real_escape_string($db,$_GET['password']);
        $image=mysqli_real_escape_string($db,$_GET['image']);
        $type=mysqli_real_escape_string($db,$_GET['type']);
        $created_at=date('Y/m/d H:i:s');
        $updated_at=date('Y/m/d H:i:s');
        $password=hash('md5',$password,false);
        $response=UsersController::register($name,$apartment,$contact,$password,$image,$type,$created_at,$updated_at);
        echo($response);
    
    }
    
    if($action==='showuser'){
        $db=getConnection();
        $id=mysqli_real_escape_string($db,$_GET['id']);
        $response=UsersController::show($id);
        echo($response);
    }
    
    if($action==='showusers'){
        $response=UsersController::index();
        echo($response);
    
    }
    
    if($action==='deleteuser'){
        $db=getConnection();
        $id=mysqli_real_escape_string($db,$_GET['id']);
        $response=UsersController::deleteUser($id);
        echo($response);
    }
    
    if($action==='validateuser'){
        $db=getConnection();
        $username=mysqli_real_escape_string($db,$_GET['contact']);
        $password=mysqli_real_escape_string($db,$_GET['password']);
        $password=hash('md5',$password,false);
        $response=UsersController::validateUser($username,$password);
        echo($response);
    }
    //end of matters users


    //get all complains
    if($action==='home'){
        $apartment=mysqli_real_escape_string($_GET['apartment']);
        $db=getConnection();
        $complains=ComplainsController::show($apartment);
        $response=array();

        foreach($complains as $complain){
            $id=$complain['id'];
            $comments=ComplainCommentsController::show($id);
            $upvotes=ComplainUpvotesController::show($id);
            $reply=RepliesController::show($id);
            $details=array('complain'=>$complain,'comments'=>$comments,'upvotes'=>$upvotes,'reply'=>$reply);
            $response[]=$details;}
            echo(json_encode($response));
    }

    if($action==='announcements'){

        $db=getConnection();
        $apartment=mysqli_real_escape_string($_GET['apartment']);
        $announcement=AnnouncementsController::show($apartment);
        echo($announcement);

    }







}    

index();

?>