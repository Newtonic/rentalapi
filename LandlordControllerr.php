<?php

namespace rental;
require_once("connect.php");
require_once("Controller.php");

class LandlordController{

    public function index(){
        $connection=getConnection();
        $sql='SELECT * FROM landlords';
        $results=$connection->query($sql);
        $response=getArray($results);
        return json_encode(array('status'=>'true','data'=>$response));
    }

    public function show($id){
        $connection=getConnection();
        $sql="SELECT * FROM landlords WHERE id='$id'";
        $results=$connection->query($sql);
        $response=getArray($results);
        return json_encode(array('status'=>'true','data'=>$response));
    }


    public function addNew($name,$contact,$password,$image,$created_at,$updated_at){
        $connection=getConnection();
        $sql="INSERT INTO landlords (name,contact,password,image,created_at,updated_at)
        VALUES ('$name','$contact','$password','$image','$created_at','$updated_at')";
        $results=$connection->query($sql);
        if($results){
            $id=$connection->insert_id();
            $sql="SELECT * FROM landlords WHERE id='$id'";
            $results=$connection->query($sql);
            $response=getArray($results);
            return json_encode(array('status'=>'true','data'=>$response));
        }else{
            return json_encode(array('status'=>'false','message'=>'error'));
        }

    }
}
?>