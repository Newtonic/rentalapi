<?php
namespace reental;
require_once("connect.php");
require_once("Controller.php");

class AnnouncementsController{
    
    public function index(){
        $connection=getConnection();
        $sql='SELECT * FROM announcements ORDER BY id DESC';
        $results=$connection->query($sql);
        $response=getArra($results);
        $connection->close();
        return json_encode(array('status'=>'true','data'=>$response));
    }


    public function show($id){
        $connection=getConnection();
        $sql="SELECT * FROM announcements WHERE id='$id' ORDER BY id DESC";
        $results=$connection->query($sql);
        $response=getArray($results);
        return json_encode(array('status'=>'true','data'=>$response));

    }


    public function addNew($content,$enddate,$userid,$apartment,$created_at,$updated_at){
        $connection=getConnection();
        $sql="INSERT INTO announcements(content,enddate,userid,apartment,created_at,updated_at)
        VALUES ('$connection','$enddate','$userid','$apartment','$created_at','$updated_at')";
        $results=$connection->query($sql);
        if($results){
        $id=$connection->insert_id();
        $sql="SELECT * FROM announcements WHERE id='$id'";
        $results=$connection->query($sql);
        $response=getArray($results);
        return json_encode(array('status'=>'true','data'=>$response));}
        else{
            return json_encode(array('status'=>'false','message'=>'an error occurred'));
        }
        

    }

    public function delete($id){
        $connection=getConnection();
        $sql="DELETE FROM announcements WHERE id='$id'";
        $results=$connection->query($sql);
        return json_encode(array('status'=>'true','data'=>array('message'=>'success')));
    }
}


?>