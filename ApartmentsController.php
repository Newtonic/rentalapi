<?php 
namespace rental;
require_once("connect.php");
require_once("Controller.php");


class ApartmentsController{

    public function index(){
        $connection=getConnection();
        $sql='SELECT * FROM apartments';
        $results=$connection->query($sql);
        $response=getArray($results);
        return json_encode(array('status'=>'true','data'=>$response));

    }

    public function show($id){
        $connection=getConnection();
        $sql="SELECT * FROM apartments WHERE id='$id'";
        $results=$connection->query($sql);
        $response=getArray($results);
        return json_encode(array('status'=>'true','data'=>$response));
    }

    public function addNew($name,$ownerid,$location,$image,$contact){
        $connection=getConnection();
        $sql="INSERT INTO apartments (name,landlord,location,image,contact) VALUES ('$name','$ownerid','$location','$image','$contact')";
        $results=$connection->query($sql);
        if($results){
            $id=$connection->insert_id();
            $sql="SELECT * FROM apartments WHERE id='$id'";
            $results=$connection->query($sql);
            $response=getArray($results);
            return json_encode(array('status'=>'true','data'=>$response));
        } else{
            return json_encode(array('status'=>'false','message'=>'an error occurred'));
        }

    }

}


?>