<?php
namespace rental;
require_once("connect.php");
require_once('Controller.php');

class UsersController{

    public function index(){
        $connect=getConnection();
        $sql='SELECT * FROM users';
        $result=$connect->query($sql);
        $response=getArray($result);
        return (json_encode(array('status'=>'true','data'=>$response)));

    }
    public function show($id){
        $connect=getConnection();
        $sql="SELECT * FROM users WHERE id='$id'";
        $result=$connect->query($sql);
        $response=getArray($result);
        return json_encode(array('status'=>'true','data'=>$response));
    }

    public function register($name,$apartment,$contact,$password,$image,$type,$created_at,$updated_at){
        $connect=getConnection();
        $sql="INSERT INTO users (name,apartment,contact,password,image,type,created_at,updated_at)
        VALUES ('$name','$apartment','$contact','$password','$image','$type','$created_at','$updated_at')";
        $result1=$connect->query($sql);
        if($result1){
            $sql="SELECT * FROM users WHERE contact='$contact'";
            $result=$connect->query($sql);
            $response=getArray($result);
            return json_encode(array('status'=>'true','data'=>$response));
        } else{
            $response=$connect->error;
            return json_encode(array('status'=>'true','data'=>$response));
        }
    }

    public function deleteUser($id){
        $connect=getConnection();
        $sql="DELETE  FROM users WHERE id='$id'";
        $result=$connect->query($sql);
        if($result){
        return json_encode(array('status'=>'true','message'=>'success'));
        } else{
            $response=$connect->error;
            return json_encode(array('status'=>'false','message'=>$response));
        }
    }


    public function validateUser($contact,$password){
        $connect=getConnection();
        $sql="SELECT * FROM users WHERE contact='$contact' AND password='$password'";
        $result=$connect->query($sql);
        if($result){
        $response=getArray($result);
        if(count($response)>0){
        return json_encode(array('status'=>'true','data'=>$response));
        } else{
            return json_encode(array('status'=>'false','message'=>'incorrect username and password combination'));
        } 
        }
        
    }
}
?>